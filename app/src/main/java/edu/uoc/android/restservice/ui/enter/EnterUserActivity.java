package edu.uoc.android.restservice.ui.enter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.ui.details.UserDetailsActivity;

public class EnterUserActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUser;
    private Button btnFollowers;

    public static final String EXTRA_USERNAME = "edu.uoc.android.restservice.UserName";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_user);
        initViews();
    }

    private void initViews() {
        etUser = findViewById(R.id.enter_user_edit_text);
        btnFollowers = findViewById(R.id.enter_user_button);
        btnFollowers.setOnClickListener(this);
    }

    private void startUserDetailsActivity() {
        /** Create an intent to go to UserDetailsActivity **/
        Intent intent = new Intent(this, UserDetailsActivity.class);
        /** Get user's name from input field and add it to the intent **/
        intent.putExtra(EXTRA_USERNAME, this.etUser.getText().toString());
        /** Show a log message **/
        Log.i("RestServiceApp", "Start the UserDetailsActivity for the user name: " + intent.getStringExtra(EXTRA_USERNAME));
        /** Start the UserDetailsActivity **/
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        if (v == btnFollowers) {
            this.startUserDetailsActivity();
        }
    }
}
