package edu.uoc.android.restservice.ui.details;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Follower;
import edu.uoc.android.restservice.rest.model.Owner;
import edu.uoc.android.restservice.ui.enter.EnterUserActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserDetailsActivity extends AppCompatActivity {

    // Views
    private View content;
    private ProgressBar progressBar;
    private ImageView userPhoto;
    private TextView userName;
    private TextView userRepositoriesValue;
    private TextView userFollowingValue;
    private RecyclerView followersList;

    // Rest service
    private Call<Owner> callOwner;
    private Call<List<Follower>> callFollowers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        initViews();
        showLoading();
        getUserData(extractUserNameFromIntent());
    }

    private void initViews(){
        /** Set views from activity_user_details.xml to this class **/
        content = findViewById(R.id.user_details_content);
        progressBar = findViewById(R.id.progressBar);
        userPhoto = findViewById(R.id.userPhoto);
        userName = findViewById(R.id.userName);
        userRepositoriesValue = findViewById(R.id.userRepositoriesValue);
        userFollowingValue = findViewById(R.id.userFollowingValue);
        followersList = findViewById(R.id.followersList);
    }

    private String extractUserNameFromIntent() {
        /** Get user's name from the intent **/
        String userNameString = this.getIntent().getStringExtra(EnterUserActivity.EXTRA_USERNAME);
        /** Set user's name in its view **/
        userName.setText(userNameString);
        return userNameString;
    }

    private void getUserData(final String userNameString) {
        callOwner = new GitHubAdapter().getOwner(userNameString);
        callOwner.enqueue(new Callback<Owner>() {
            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                Owner owner = response.body();
                if(owner != null) {
                    Picasso.get().load(owner.getAvatarUrl()).into(userPhoto);
                    userRepositoriesValue.setText(owner.getPublicRepos().toString());
                    userFollowingValue.setText(owner.getFollowing().toString());
                    getUserFollowers(userNameString);
                } else {
                    showError("Got an empty owner");
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable throwable) {
                showError("An error occurred getting the user's data");
            }
        });
    }

    private void getUserFollowers(String userNameString) {
        callFollowers = new GitHubAdapter().getFollowers(userNameString);
        callFollowers.enqueue(new Callback<List<Follower>>() {
            @Override
            public void onResponse(Call<List<Follower>> call, Response<List<Follower>> response) {
                List<Follower> listFollowers = response.body();
                if(listFollowers != null) {
                    FollowerAdapter followerAdapter = new FollowerAdapter(listFollowers);
                    followersList.setAdapter(followerAdapter);
                    showContent();
                } else {
                    showError("Got an empty followers list");
                }
            }

            @Override
            public void onFailure(Call<List<Follower>> call, Throwable throwable) {
                showError("An error occurred getting the user's followers");
            }
        });
    }

    private void showLoading() {
        content.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showContent() {
        progressBar.setVisibility(View.GONE);
        content.setVisibility(View.VISIBLE);
    }

    private void showError(String message) {
        /** Show a log message **/
        Log.i("RestServiceApp", message);
    }
}
